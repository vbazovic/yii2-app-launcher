# INSTALLATION #

Install it from composer:

`composer require vbazovic/yii2-app-launcher @dev`

### USAGE ###

Example:
~~~~
<?= \vbazovic\launcher\AppLauncher::widget([
                    'defaultState' => 'none',
                    'drawerPosition' => \vbazovic\launcher\AppLauncher::AL_RIGHT,  // AL_RIGHT, AL_LEFT, AL_CENTER (default)
                    'options' => [
                        'class' => 'my-launcher',
                        'id' => 'my-id',
                    ],
                    'appEvenMoreApps' => 'http://my-more-apps.com',
                    'items'=>[
                        'title dropbox'=>['link'=>'http://dropbox.com','content'=>'<span class="glyphicon glyphicon-file"><br/>Dropbox</span>'],
                        'title controller/action'=>['link'=>'site/about','content'=>'<span class="glyphicon glyphicon-plus-sign"><br/></span>'],
                        'style'=>['link'=>'some-link','content'=>'<span style="background-color: blue" class="glyphicon glyphicon-minus-sign"><br/>aaaa</span>'],
                        'another title 1'=>['link'=>'link-1','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 2'=>['link'=>'link-2','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 3'=>['link'=>'link-3','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 4'=>['link'=>'link-4','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 5'=>['link'=>'link-5','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 6'=>['link'=>'link-6','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 7'=>['link'=>'link-7','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 8'=>['link'=>'link-8','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 9'=>['link'=>'link-9','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 10'=>['link'=>'link-10','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        'another title 11'=>['link'=>'link-11','content'=>'<span class="glyphicon glyphicon-minus-sign"></span>'],
                        ]])?>
~~~~

### CUSTOMIZATION ###
Put in your css file:

~~~~
/* size of a element */
.my-launcher {
  width: 55px !important;
}

/* green point to the left  */
.my-launcher > .app-launcher::before {
    left: 9px !important;
    border-bottom: 10px solid rgba(0,141,76, 0.8) !important;
}
~~~~


* 0.9.0
* [Report a problem](https://bitbucket.org/vbazovic/yii2-app-launcher/issues?status=new&status=open)