<?php

/**
 * @package   app-launcher
 * @author    Vladimir Bazović <vbazovic@gmail.com>
 * @copyright Copyright &copy; 2017
 * @version   0.0.0
 */

namespace vbazovic\launcher;

use Yii;
use yii\base\Widget as YiiWidget;
use yii\helpers\Url;
use yii\base\Exception;

/**
 * Class for app-launcher widget.
 * Adapted and changed for yii2 from https://codepen.io/ManarKamel/pen/ecypH 
 *
 * @author Vladimir Bazović <vbazovic@gmail.com>
 * @since  1.0
 */
class AppLauncher extends YiiWidget {

    
    const AL_CENTER = 0;
    const AL_LEFT = 1;
    const AL_RIGHT = 2;
    
    const DEFAULT_APP_IN_SET_NUM = 9;
    const DEFAULT_APP_MAX_NUM = 18;
    const SECOND_SET_EMPTY_VALUE = '<ul class="second-set hide"></ul>';

    /**
     *
     * @var int drawer position, defaults to center
     */
    public $drawerPosition = self::AL_CENTER;
    /**
     *
     * @var string defaultState of app drawer (hide or show)
     */
    public $defaultState = 'hide';
    /**
     * @var integer  First set number of applications 
     */
    public $appInSetNum = 0;
    /**
     * @var integer Max number of applications, defaults to 18 
     */
    public $appMaxNum = 0;
    /**
     * @var string  Link to more apps
     */
    public $appEvenMoreApps = '';

    /**
     * @var string Icon for launcher, defaults to glyphicons-th
     */
    public $appLauncherIcon = '';

    /**
     * @var array HTML attributes or other settings for widgets (class && id).
     */
    public $options = [];

    /**
     * @var array You must define items in `item-title => item-link` format. Item-link is in format ['link'=> link-value, 'content' => link-content] For example:
     * items = [
     *     'objects index' => ['link' => 'object/index', 'content' => '<span class="glyphicon glyphicon-plus-sign"></span>'],
     *     ...
     * ];
     */
    public $items = [];

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();
        if (empty($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }        
        
        if (empty($this->appInSetNum)) {
            $this->appInSetNum = self::DEFAULT_APP_IN_SET_NUM;
        }
        
        if (empty($this->defaultState)) {
            $this->defaultState = 'none';
        }
        if (empty($this->appMaxNum)) {
            $this->appMaxNum = self::DEFAULT_APP_MAX_NUM;
        }
        if (empty($this->appLauncherIcon)) {
            $this->appLauncherIcon = '<i class="glyphicon glyphicon-th"></i>';
        }
    }

    protected function firstSet() {
        $html = '<ul class="first-set">';
        for ($i = 0; ($i < count($this->items) and $i < self::DEFAULT_APP_IN_SET_NUM); $i++) {
            $c = current($this->items);
            $html .= '<li><a title="' . key($this->items) . '" href="' . $c['link'] . '" class="app-launcher-link">' . $c['content'] . '</a></li>';
            next($this->items);
        }
        $html .= '</ul>';
        return $html;
    }

    protected function secondSet() {
        $html = '<ul class="second-set hide">';
        for ($i = 0; ($i < count($this->items) - self::DEFAULT_APP_IN_SET_NUM and $i < self::DEFAULT_APP_MAX_NUM); $i++) {
            $c = current($this->items);
            $html .= '<li><a title="' . key($this->items) . '" href="' . Url::to($c['link']) . '" class="app-launcher-link">' . $c['content'] . '</a></li>';
            next($this->items);
        }
        $html .= '</ul>';
        return $html;
    }

    public function run() {

        Yii::setAlias('@vbazovic', '@vendor/vbazovic');

        if (empty($this->items)) {
            throw new Exception(Yii::t('app', 'Items not defined'));
        }

        // must be called first
        $firstSet = $this->firstSet();
        
        return $this->render(
                'AppLauncherView', [
                    'defaultState' => $this->defaultState,
                    'appLauncherIcon' => $this->appLauncherIcon,
                    'appEvenMoreApps' => $this->appEvenMoreApps,
                    'options' => $this->options,
                    'firstSet' => $firstSet,
                    'secondSet' => $this->secondSet(),
                    'drawerPosition' => $this->drawerPosition,
                ]
        );
    }

}
