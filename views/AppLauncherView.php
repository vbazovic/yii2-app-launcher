<?php

use yii\helpers\Html;
use \vbazovic\launcher\AppLauncherAsset;
// Register asset bundle
AppLauncherAsset::register($this);

switch ($drawerPosition) {
    case vbazovic\launcher\AppLauncher::AL_LEFT:
        $css_add = 'app-launcher app-launcher-l';
        break;

    case vbazovic\launcher\AppLauncher::AL_RIGHT:
        $css_add = 'app-launcher app-launcher-r';
        break;
    
    default:
        $css_add = 'app-launcher';
        break;
}

// [BEGIN] - App launcher widget container
echo Html::beginTag(
    'div',
    [
        'class' => 'launcher'. ' '.(isset($options['class'])?$options['class']:''),        
        'id' => $options['id'],        
    ]
);

    // 3x3 button
    echo Html::tag(
        'div',
        $appLauncherIcon,
        [
            'class' => 'button',
        ]
    );

    echo Html::beginTag(
        'div',
        [
            'class' => $css_add,
            'style' => 'display:'. $defaultState,
        ]
    );
        echo Html::beginTag(
            'div',
            [
                'class' => 'apps',                        
            ]
        );     
            // must be called first
            echo $firstSet;
            if ($secondSet !== \vbazovic\launcher\AppLauncher::SECOND_SET_EMPTY_VALUE) {
                echo Html::a(Yii::t('app','More'),'#',['class' => 'app-launcher-more']);            
            }
            echo $secondSet;            
            if (!empty($appEvenMoreApps)) {               
                echo Html::a(Yii::t('app','Even more apps'), $appEvenMoreApps ,['class' => 'app-launcher-even-more hide']);
            }
        echo Html::endTag('div');
    echo Html::endTag('div');
echo Html::endTag('div');

