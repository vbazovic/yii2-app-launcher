<?php
namespace vbazovic\launcher;

use Yii;
use yii\web\AssetBundle;

class AppLauncherAsset extends AssetBundle {       
    public $sourcePath = '@vendor/vbazovic/yii2-app-launcher';
    public $css = [
        'launcher.css'
    ];
    public $js = [
        'launcher.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    public $jsOptions =
    [
        'position' => \yii\web\View::POS_END,
    ];            
}